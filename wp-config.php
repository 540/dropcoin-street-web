<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', wordpress);

/** MySQL database username */
define('DB_USER', wordpress);

/** MySQL database password */
define('DB_PASSWORD', wordpress);

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         'Cm!nbb}=zm*saJN4yK.m[+%!f25:W5l+a,5:-KgxZL~<oY8VS% }Qm$Ldr0+_g<^');
define('SECURE_AUTH_KEY',  'W+(;f[)l]q~I,3<W|b+twLbjtpD| l+k`h2TCm<qkC0TX#Zx~l(mN_+=GH]0$WN=');
define('LOGGED_IN_KEY',    'KLy$v!gmV ^5~}Kb:.VH.8J7[1o[ohK:bZu5$J#tbV5vn$0w#!/Y`dZ.OCRm,=io');
define('NONCE_KEY',        'I-Tg$bjrC%4xY6tmu8rGgj(@f9O8X)-;R-{X(80jm|AS-k1&vw%dx27HI5ggW|TU');
define('AUTH_SALT',        'AMt!L~d5FEoWaKKAsR-ncO|sP=(g+E%Nlf0eGn<n2nyD8A:Ssk>8)Y$abc[{Ke;7');
define('SECURE_AUTH_SALT', '>K[=wbNCmP&-w:9lJqk 6wtr^||=IZ#CEaH1w!_i7U46<Q f+i923v@2Hh]%n?W9');
define('LOGGED_IN_SALT',   '$zpi.OpU0=pv05]kH0|=*0rw.W0@%/VJ.q`|NJuSogdhg}ZfwkjV(^sZ.HK)/*~H');
define('NONCE_SALT',       'O|-U*aozF.Z5&wph9&c-.#=uR2Kn+Z].lM-/VTnvCzArXKEvj,#ag<UfSt*c%kZ$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
